//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Sandip Agrawal on 2/28/13.
//  Copyright (c) 2013 Sandip Agrawal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
