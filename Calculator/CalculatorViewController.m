//  Created by Sandip Agrawal on 2/28/13.
//  Copyright (c) 2013 Sandip Agrawal. All rights reserved.

#import "CalculatorViewController.h"

@interface CalculatorViewController ()

@property (nonatomic) BOOL typingNumber;
@property (nonatomic) NSNumber * storedNumber;
@property (nonatomic) NSString * operand;

- (double) operateVal:(double)left withOp:(NSString *)op onVal:(double)right;
- (void) setDisplayVal:(double)val;

@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize typingNumber = _typingNumber;
@synthesize storedNumber = _storedNumber;
@synthesize operand = _operand;

- (IBAction)numberPressed:(UIButton *)sender {
    if (self.typingNumber) {
        self.display.text = [self.display.text stringByAppendingString:sender.currentTitle];
    } else {
        self.display.text = sender.currentTitle;
        self.typingNumber = YES;
    }
}
- (IBAction)whenDotPressed {
    if(self.typingNumber){
        if([self.display.text rangeOfString:@"."].location == NSNotFound){
            self.display.text = [self.display.text stringByAppendingString:@"."];
        }
    } else {
        self.display.text = @"0.";
        self.typingNumber = YES;
    }
    
}

- (IBAction)opsPressed:(UIButton *)sender { 
    if(self.operand && self.storedNumber && self.typingNumber){
        double result = [self operateVal:[self.storedNumber doubleValue] withOp:self.operand onVal:[self.display.text doubleValue]];
        self.storedNumber = [NSNumber numberWithDouble:result];
        [self setDisplayVal:result];
    } else if(self.typingNumber) {
        self.storedNumber = [NSNumber numberWithDouble:[self.display.text doubleValue]];
    }
    
    self.operand = sender.currentTitle;
    self.typingNumber = NO;
    
    
}
- (IBAction)clearState {
    self.storedNumber = nil;
    self.operand = nil;
    self.display.text = @"0";
    self.typingNumber = NO;
}

- (IBAction)equalPressed:(UIButton *)sender {
    if (self.storedNumber && self.operand && self.typingNumber) {
        double newNumber = [self.display.text doubleValue];
        double result = [self operateVal:[self.storedNumber doubleValue] withOp:self.operand onVal:newNumber];
        self.storedNumber = [NSNumber numberWithDouble:result];
        self.operand = nil;
        self.typingNumber = NO;
        [self setDisplayVal:result];
    }
}

- (void) setDisplayVal:(double)val{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.###"];
    self.display.text = [fmt stringFromNumber:[NSNumber numberWithDouble:val]];
}

- (double) operateVal:(double)left withOp:(NSString *)op onVal:(double)right{
    if([op isEqualToString:@"+"]){
        return left + right;
    } else if([op isEqualToString:@"-"]){
        return left - right;
    } else if([op isEqualToString:@"/"]){
        return left / right;
    } else if([op isEqualToString:@"*"]){
        return left * right;
    }
}

@end
